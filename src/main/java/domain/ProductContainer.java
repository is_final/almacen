package domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "productContainer")

public class ProductContainer implements BaseEntity<Long>{
	
	@Id
	@SequenceGenerator(name = "pcategory_id_generator", sequenceName = "pcategory_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pcategory_id_generator")
	private Long id;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String name;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private float height;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private float wight;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private float weigth;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String classification;
	
	@OneToMany(mappedBy = "pcontainer")
	private Collection<Product> pc;	
	
	@ManyToOne
	private ProviderProductContainer productContainer;
	
	@ManyToOne
	private DeliveryNote dnpcontainer;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}
