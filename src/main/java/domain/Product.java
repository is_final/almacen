package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "product")

public class Product implements BaseEntity<Long>{
	
	@Id
	@SequenceGenerator(name = "pcategory_id_generator", sequenceName = "pcategory_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pcategory_id_generator")
	private Long id;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String name_product;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String category;
	
	@ManyToOne
	private ProductCategory pcategory;
	
	@ManyToOne
	private ProductContainer pcontainer;
	
	@ManyToOne
	private DeliveryNote dnproduct;
	
	public void add_product(){
	}
	public String category(){
		return category;
	}
	@Override
	public Long getId() {
		return id;
	}
	@Override
	public void setId(Long id) {
		this.id = id;
	}
}
