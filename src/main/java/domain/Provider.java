package domain;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "provider")
public class Provider implements BaseEntity<Long>{
	
	@Id
	@SequenceGenerator(name = "provider_id_generator", sequenceName = "provider_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "provider_id_generator")
	private Long id;
	
	@OneToMany(mappedBy = "provider")
	private Collection<Business> bss;
	
	@OneToMany(mappedBy = "providerd")
	private Collection<ProviderProductContainer> ppc;
	
	public void list_product(){
	}
	public boolean confirm(){
		return true;
	}
	@Override
	public Long getId() {
		return id;
	}
	@Override
	public void setId(Long id) {
		this.id = id;
	}
}
