package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "business")
public class Business implements BaseEntity<Long>{

	@Id
	@SequenceGenerator(name = "account_id_generator", sequenceName = "account_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_id_generator")
	private Long id;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String name_business;
	
	@Column(unique = true, nullable = false, updatable = false, length = 120)
	private String document_business;
	
	@Column(unique = true, nullable = false, updatable = false, length = 40)
	private String address;
	
	@ManyToOne
	private Persona owner;
	
	@ManyToOne
	private Provider provider;
	
	public Business(){
	}
	
	public Business(String name_business, String document_business, String address){
		this.name_business = name_business;
		this.document_business = document_business;
		this.address = address;
	}
	
	@Override
	public Long getId(){
		return id;
	}
	
	@Override
	public void setId(Long id){
		this.id = id;
	}
	
	public String getName_Business(){
		return name_business;
	}
	
	public void setName_Business(String name_business){
		this.name_business = name_business;
	}
	
	public String getDocument_Business(){
		return document_business;
	}
	
	public void setDocument_Business(String document_business){
		this.document_business = document_business;
	}
	
	public String getAddress(){
		return address;
	}
	
	public void setAddress(String address){
		this.address = address;
	}
		
}