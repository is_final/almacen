package domain;

import domain.Row;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "shelf")

public class Shelf implements BaseEntity<Long>{
	
	@Id
	@SequenceGenerator(name = "shelf_id_generator", sequenceName = "shelf_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "shelf_id_generator")
	private Long id;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String name;
	
	@ManyToOne
	private Row row;
	
	@OneToMany(mappedBy = "shelf")
	private Collection<ShelfLevel> sl;	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Row getRow() {
		return row;
	}
	
	public void setRow(Row row) {
		this.row = row;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	/*@Override
	public String toString(){
		return this.row + "Shelf: " + this.name + " ";
	}*/

}
