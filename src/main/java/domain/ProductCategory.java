package domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "productCategory")

public class ProductCategory implements BaseEntity<Long>{
	
	@Id
	@SequenceGenerator(name = "pcategory_id_generator", sequenceName = "pcategory_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pcategory_id_generator")
	private Long id;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String name_pcategory;
	
	@OneToMany(mappedBy = "pcategory")
	private Collection<Product> pd;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}
