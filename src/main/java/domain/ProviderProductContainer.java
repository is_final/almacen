package domain;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ppcontainer")

public class ProviderProductContainer implements BaseEntity<Long>{
	
	@Id
	@SequenceGenerator(name = "ppcontainer_id_generator", sequenceName = "ppcontainer_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ppcontainer_id_generator")
	private Long id;
	
	@OneToMany(mappedBy = "productContainer")
	private Collection<ProductContainer> productContainer;
	
	@ManyToOne
	private Provider providerd;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}
