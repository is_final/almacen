package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "deliveryNote")

public class DeliveryNote implements BaseEntity<Long>{
	
	@Id
	@SequenceGenerator(name = "pcategory_id_generator", sequenceName = "pcategory_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pcategory_id_generator")
	private Long id;
	
	@Column(unique = true, nullable = false, updatable = false, length = 40)
	private String provider_product_container;
	
	@Column(unique = true, nullable = false, updatable = false, length = 40)
	private int quantity;
	
	@Column(unique = true, nullable = false, updatable = false, length = 40)
	private Date created = new Date(2015);
	
	@Column(unique = true, nullable = false, updatable = false, length = 40)
	private int price;
	
	@ManyToOne
	private Kardex dnkardex;
	
	@OneToMany(mappedBy = "dnproduct")
	private Collection<Product> dnp;
	
	@OneToMany(mappedBy = "dnpcontainer")
	private Collection<ProductContainer> dnpc;
	
	public String issue_voucher(){
		return provider_product_container;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
}
