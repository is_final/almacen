package domain;

import java.sql.Date;

import javax.persistence.Column;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "kardex")

public class Kardex implements BaseEntity<Long>{
	
	@Id
	@SequenceGenerator(name = "kardex_id_generator", sequenceName = "kardex_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kardex_id_generator")
	private Long id;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String quote;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private Date created = new Date(2015);

	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String shelic;
	
	@OneToMany(mappedBy = "kardex")
	private Collection<ShelfLevel> shl;
	
	@OneToMany(mappedBy = "dnkardex")
	private Collection<DeliveryNote> dnk;
	
	void product_list(){
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	

}
