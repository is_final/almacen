package controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.PersonaService;
import domain.Persona;

@Controller
public class PersonaController {
	
	@Autowired
	PersonaService personaService;

	@RequestMapping(value = "/persona", method = RequestMethod.POST)
	String SavePersona(@ModelAttribute Persona persona, ModelMap model) {
		personaService.save(persona);
		System.out.println("saving: " + persona.getId());
		return ShowPersona(persona.getId(), model);
	}
	@RequestMapping(value = "/add-persona", method = RequestMethod.GET)
	String AddPersona(@RequestParam(required = false) Long id, ModelMap model) {
		Persona persona = id == null ? new Persona() : personaService.get(id);
		model.addAttribute("persona", persona);
		return "add-persona";
	}

	@RequestMapping(value = "/persona", method = RequestMethod.GET)
	String ShowPersona(@RequestParam(required = false) Long id, ModelMap model) {
		if (id != null) {
			Persona persona = personaService.get(id);
			model.addAttribute("persona", persona);
			return "persona";
		} else {
			Collection<Persona> people = personaService.getAll();
			model.addAttribute("people", people);
			return "people";
		}
	}
}
