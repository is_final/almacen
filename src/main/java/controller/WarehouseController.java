package controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.WarehouseService;
//import service.PersonaService;
import domain.Warehouse;
//import domain.Persona;

@Controller
public class WarehouseController {
	@Autowired
	WarehouseService warehouseService;
//	PersonaService personaService;

	@RequestMapping(value = "/warehouse", method = RequestMethod.POST)
	String SaveWarehouse(@ModelAttribute Warehouse warehouse, ModelMap model) {
		warehouseService.save(warehouse);
		System.out.println("saving: " + warehouse.getId());
		return ShowWarehouse(warehouse.getId(), model);
	}
	@RequestMapping(value = "/add-warehouse", method = RequestMethod.GET)
	String AddWarehouse(@RequestParam(required = false) Long id, ModelMap model) {
		Warehouse warehouse = id == null ? new Warehouse() : warehouseService.get(id);
		model.addAttribute("warehouse", warehouse);
//		Collection<Persona> people = personaService.getAll();
//		model.addAttribute("people", people);
		return "add-warehouse";
	}

	@RequestMapping(value = "/warehouse", method = RequestMethod.GET)
	String ShowWarehouse(@RequestParam(required = false) Long id, ModelMap model) {
		if (id != null) {
			Warehouse warehouse = warehouseService.get(id);
			model.addAttribute("warehouse", warehouse);
			return "warehouse";
		} else {
			Collection<Warehouse> warehouse = warehouseService.getAll();
			model.addAttribute("warehouses", warehouse);
			return "warehouses";
		}
	}
}
