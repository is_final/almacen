package repository;

import domain.Persona;

public interface PersonaRepository extends BaseRepository<Persona, Long> {

}
