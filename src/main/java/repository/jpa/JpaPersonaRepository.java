package repository.jpa;

import org.springframework.stereotype.Repository;

import repository.PersonaRepository;
import domain.Persona;

@Repository
public class JpaPersonaRepository extends JpaBaseRepository<Persona, Long> implements PersonaRepository {
}
