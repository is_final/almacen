package repository.jpa;

import org.springframework.stereotype.Repository;

import repository.WarehouseRepository;
import domain.Warehouse;

@Repository
public class JpaWarehouseRepository extends JpaBaseRepository<Warehouse, Long> implements WarehouseRepository {
}
