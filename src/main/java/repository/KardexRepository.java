package repository;

import java.util.Collection;

import org.springframework.stereotype.Repository;
import domain.Kardex;

@Repository
public interface KardexRepository extends BaseRepository<Kardex, Long>{
	
	Collection<Kardex> findByWarehouse(Long kardexId);

}
