package repository;

import domain.Warehouse;

public interface WarehouseRepository extends BaseRepository<Warehouse, Long> {

}
