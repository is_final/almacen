package repository;

import domain.Business;

import java.util.Collection;

import org.springframework.stereotype.Repository;

@Repository
public interface BusinessRepository extends BaseRepository<Business, Long> {
	
	Collection<Business> findByBusiness();

}
