package service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.WarehouseRepository;
import domain.Warehouse;

@Service
public class WarehouseService {
	@Autowired
	WarehouseRepository warehouseRepository;
	
	@Transactional
	public void save(Warehouse warehouse) {
		if (warehouse.getId() == null) {
			warehouseRepository.persist(warehouse);
		} else {
			warehouseRepository.merge(warehouse);
		}
	}

	public Warehouse get(Long id) {
		return warehouseRepository.find(id);
	}

	public Collection<Warehouse> getAll() {
		return warehouseRepository.findAll();
	}
}
