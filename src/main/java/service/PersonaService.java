package service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.PersonaRepository;
import domain.Persona;

@Service
public class PersonaService {

	@Autowired
	PersonaRepository personaRepository;

	@Transactional
	public void save(Persona persona) {
		if (persona.getId() == null) {
			personaRepository.persist(persona);
		} else {
			personaRepository.merge(persona);
		}
	}

	public Persona get(Long id) {
		return personaRepository.find(id);
	}

	public Collection<Persona> getAll() {
		return personaRepository.findAll();
	}
}